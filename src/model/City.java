package model;

import javax.persistence.*;

import org.jetbrains.annotations.NotNull;

@Entity
@Table(name = "city")
public class City {

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;

	@Column
	private String location;
	@Column
	@NotNull
	private double longitude;
	@Column
	@NotNull
	private double latitude;

	public City() {
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
