package model;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column
	@NotNull
	private String name;

	@Column
	@NotNull
	private String email;

	@Column
	@NotNull
	private String passw;
	
	@Column
	@NotNull
	private String permisions;
	
	@ManyToMany(mappedBy = "pasagers",fetch=FetchType.EAGER)
	private List<Fly> flights;

	public User() {
	}

	public String getPermisions() {
		return permisions;
	}

	public void setPermisions(String permisions) {
		this.permisions = permisions;
	}

	public User(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassw() {
		return passw;
	}

	public void setPassw(String passw) {
		this.passw = passw;
	}

	public List<Fly> getFlights() {
		return flights;
	}

	public void setFlights(List<Fly> flights) {
		this.flights = flights;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email=" + email + ", passw=" + passw + ", flights=" + flights
				+ "]";
	}


}
