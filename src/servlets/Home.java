package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import accessDB.EntityDatabaseAccess;
import model.Fly;
import model.User;

/**
 * Servlet implementation class Home
 */
@WebServlet("/home")
public class Home extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Home() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EntityDatabaseAccess eda = new EntityDatabaseAccess();
		boolean isUserAutheticated = false;
		int cookieID = 0;
		try {
			for (Cookie cook : request.getCookies()) {
				if (cook.getName().equals("SESSION")) {
					cookieID = Integer.parseInt(cook.getValue());
					User us = eda.getUserById(Integer.parseInt(cook.getValue()));
					if (us == null) {
						RequestDispatcher requestDispatcher = request.getRequestDispatcher("/hello.jsp");
						requestDispatcher.forward(request, response);
					}
				}
			}
		} catch (Exception e) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/hello.jsp");
			requestDispatcher.forward(request, response);
		}
		User us = eda.getUserById(cookieID);
		System.out.println("merge");
		if (response.getHeader("UTYPE").equals("ADMIN")) {
			ArrayList<Fly> allFly = new ArrayList<Fly>();

			User d1 = new User();
			d1.setName("Test1");

			User d2 = new User();
			d2.setName("test2");

			User d3 = new User();
			d3.setName("test3");
			allFly = (ArrayList<Fly>) eda.viewAllFlys();
			request.setAttribute("allFly", allFly);
			request.setAttribute("status", "200");
			System.out.println("ACUM SUNT ADMIN");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/overview.jsp");
			requestDispatcher.forward(request, response);
		}
		if (response.getHeader("UTYPE").equals("PASSENGER")) {
			ArrayList<Fly> allFly = new ArrayList<Fly>();
			System.out.println(us.getId());
			allFly = (ArrayList<Fly>) eda.allFlightsOfOneUser(us.getId());
			request.setAttribute("allFly", allFly);
			request.setAttribute("status", "200");
			request.setAttribute("userName", us.getName());
			request.setAttribute("name", us);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/client.jsp");
			requestDispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EntityDatabaseAccess eda = new EntityDatabaseAccess();
		System.out.println(request.getParameter("email"));
		System.out.println(request.getParameter("password"));
		try {
			User u = eda.getUserByEmailAndPassword(request.getParameter("email"), request.getParameter("password"));
			System.out.println(u.toString());
			int idUser = u.getId();
			Cookie cookie = new Cookie("SESSION", String.valueOf(idUser));
			cookie.setMaxAge(600);
			response.addCookie(cookie);
			if(u.getPermisions().equals("ADMIN")) {
				response.addHeader("UTYPE", "ADMIN");
			}
			if(u.getPermisions().equals("PASSENGER")) {
				response.addHeader("UTYPE", "PASSENGER");
			}
			doGet(request, response);
		} catch (Exception e) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/hello.jsp");
			requestDispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doOptions(HttpServletRequest, HttpServletResponse)
	 */
	protected void doOptions(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
