package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import accessDB.EntityDatabaseAccess;
import model.Fly;
import model.User;

/**
 * Servlet implementation class Overview
 */
@WebServlet("/overview")
public class Overview extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Overview() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EntityDatabaseAccess eda = new EntityDatabaseAccess();
		ArrayList<Fly> allFly = new ArrayList<Fly>();

		User d1 = new User();
		d1.setName("Test1");

		User d2 = new User();
		d2.setName("test2");

		User d3 = new User();
		d3.setName("test3");
		allFly = (ArrayList<Fly>) eda.viewAllFlys();
		request.setAttribute("allFly", allFly);
		request.setAttribute("status", "200");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EntityDatabaseAccess eda = new EntityDatabaseAccess();
		Fly f = new Fly();
		f.setAirplane(request.getParameter("airplane"));
		f.setArivDate(request.getParameter("arivalDate"));
		f.setDepCity(eda.getCityByLocation(request.getParameter("dcity")));
		f.setDepDate(request.getParameter("departureDate"));
		f.setArrivCity(eda.getCityByLocation(request.getParameter("dcity")));
		eda.insertFly(f);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/overview.jsp");
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EntityDatabaseAccess eda = new EntityDatabaseAccess();
		Fly f = eda.readFly(request.getParameter("deleteNr"));
		eda.deleteFly(f);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/home");
		requestDispatcher.forward(request, response);
	}

}
