<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"
	type="text/javascript"></script>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
body {
	font-family: 'Open Sans', sans-serif;
	background: #3498db;
	margin: 0 auto 0 auto;
	width: 100%;
	text-align: center;
	margin: 20px 0px 20px 0px;
}

p {
	font-size: 12px;
	text-decoration: none;
	color: #ffffff;
}

h1 {
	font-size: 1.5em;
	color: #525252;
}

.box {
	background: white;
	width: 300px;
	border-radius: 6px;
	margin: 0 auto 0 auto;
	padding: 0px 0px 70px 0px;
	border: #2980b9 4px solid;
}

.email {
	background: #ecf0f1;
	border: #ccc 1px solid;
	border-bottom: #ccc 2px solid;
	padding: 8px;
	width: 250px;
	color: #AAAAAA;
	margin-top: 10px;
	font-size: 1em;
	border-radius: 4px;
}

.password {
	border-radius: 4px;
	background: #ecf0f1;
	border: #ccc 1px solid;
	padding: 8px;
	width: 250px;
	font-size: 1em;
}

.btn {
	background: #2ecc71;
	width: 125px;
	padding-top: 5px;
	padding-bottom: 5px;
	color: white;
	border-radius: 4px;
	border: #27ae60 1px solid;
	margin-top: 20px;
	margin-bottom: 20px;
	float: left;
	margin-left: 16px;
	font-weight: 800;
	font-size: 0.8em;
}

.btn:hover {
	background: #2CC06B;
}

#btn2 {
	float: left;
	background: #3498db;
	width: 125px;
	padding-top: 5px;
	padding-bottom: 5px;
	color: white;
	border-radius: 4px;
	border: #2980b9 1px solid;
	margin-top: 20px;
	margin-bottom: 20px;
	margin-left: 10px;
	font-weight: 800;
	font-size: 0.8em;
}

#btn2:hover {
	background: #3594D2;
}
</style>
</head>
<body>
	<form action="home" method="post">
		<div class="box">
			<h1>Log in form</h1>
			<input type="email" name="email" value="email" class="email" /> <input
				type="password" name="password" value="password" class="email" /> <input
				type="submit" value="Login"></input>
		</div>
	</form>
</body>
</html>