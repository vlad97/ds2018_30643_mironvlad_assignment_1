<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 60%;
}

td, th {
	border: 1px solid #dddddd;
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #dddddd;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin overview</title>
</head>
<%
	Object id = request.getAttribute("l");
%>
<body>
	<p>
		<b>This are all the available flights!!</b>
	</p>
	<table width="55%">
		<tr>
			<th>Fly Number</th>
			<th>Airplane Type</th>
			<th>Arrival Date</th>
			<th>Arrival City</th>
			<th>Departure Date</th>
			<th>Departure City</th>

		</tr>
		<c:forEach items="${allFly}" var="fly">
			<tr>
				<td>${fly.flightNumber}</td>
				<td>${fly.airplane}</td>
				<td>${fly.arivDate}</td>
				<td>${fly.arrivCity.location}</td>
				<td>${fly.depDate}</td>
				<td>${fly.depCity.location}</td>
			</tr>
		</c:forEach>
	</table>
	<b>Operations on flights CRUD</b>
	<p>
		<i>Add new flight</i>
	</p>
	<form action="overview" method="post">
		Airplane type: <input type="text" name="airplane"><br>
		Arrival date: <input type="text" name="arivalDate"><br>
		Departure date: <input type="text" name="departureDate"><br>
		Destination city location: <input type="text" name="acity"><br>
		Departure city location<input type="text" name="dcity"><br>
		<input type="submit" value="Insert new flight">
	</form>
	<p>
		<i>Delete flight</i>
	</p>
	<form action="delete" method="post">
		Flight Number<input type="text" name="deleteNr"><br> <input
			type="submit" value="Delete flight">
	</form>
	<p>
		<i>Update flight</i>
	</p>
	<form action="flight" method="post">
		Flight Number<input type="text" name="updateNr"><br>Airplane
		type: <input type="text" name="airplane"><br> Arrival
		date: <input type="text" name="arivalDate"><br> Departure
		date: <input type="text" name="departureDate"><br> <input
			type="submit" value="Update flight">
	</form>
</body>
</html>